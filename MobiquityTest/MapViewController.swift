//
//  MapViewController.swift
//  Mobiquity
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import UIKit
import GoogleMaps
import GooglePlaces


class MapViewController: UIViewController , GMSMapViewDelegate, UISearchBarDelegate, GMSAutocompleteTableDataSourceDelegate {
    
    @IBOutlet weak var lblLocation: UILabel!
    private var tableView: UITableView!
    private var searchBar:UISearchBar!
    private var tableDataSource: GMSAutocompleteTableDataSource!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var strPinCode:String = ""
    var strLatitute: String = ""
    var strLongitute: String = ""
    
   weak var delegate: confirmLocationProtocol?
    let geocoder = GMSGeocoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Choose Location Here"
        mapView.delegate = self
        setupSearchController()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    func setupSearchController() {
        
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 70, width: self.view.frame.size.width, height: 44.0))
        searchBar.delegate = self
        view.addSubview(searchBar)
        
        tableDataSource = GMSAutocompleteTableDataSource()
        tableDataSource.delegate = self
        
        tableView = UITableView(frame: CGRect(x: 0, y: 114, width: self.view.frame.size.width, height: self.view.frame.size.height - 114))
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
        
        view.addSubview(tableView)
        view.addSubview(searchBar)
        
        tableView.isHidden = true
    }
    
    @IBAction func btnConfirmLocationClicked(_ sender: Any) {
        
        if delegate != nil
        {
            let cityDetail = CityDetail(address: lblLocation.text ?? "", lat: strLatitute, long: strLongitute)
            delegate?.locationSelected(cityDetail: cityDetail)
            self.navigationController?.popViewController(animated: true)
        }
    }
    // Camera change Position this methods will call every time
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocode(coordinate: position.target)
    }
    
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard
                let address = response?.firstResult(),
                let lines = address.lines
            else {
                return
            }
            
            // 3
            self.lblLocation.text = lines.joined(separator: "\n")
            self.searchBar.text = lines.joined(separator: "\n")
            self.strPinCode = address.postalCode ?? ""
            self.strLatitute = "\(address.coordinate.latitude)"
            self.strLongitute = "\(address.coordinate.longitude)"
            
            // 4
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Update the GMSAutocompleteTableDataSource with the search text.
        self.tableView.isHidden = false
        tableDataSource.sourceTextHasChanged(searchText)
    }
    
    func didUpdateAutocompletePredictions(for tableDataSource: GMSAutocompleteTableDataSource) {
        // Turn the network activity indicator off.
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        // Reload table data.
        tableView.reloadData()
    }
    
    func didRequestAutocompletePredictions(for tableDataSource: GMSAutocompleteTableDataSource) {
        // Turn the network activity indicator on.
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // Reload table data.
        tableView.reloadData()
    }
    
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didAutocompleteWith place: GMSPlace) {
        // Do something with the selected place.
        print("Place name: \(place.name ?? "")")
        print("Place address: \(place.formattedAddress ?? "")")
        // print("Place attributions: \(place.attributions ?? "")")
        searchBar.text = "\(place.formattedAddress ?? "")"
        //  CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        self.lblLocation.text = "\(place.formattedAddress ?? "")"
      //  self.strPinCode = place.addressComponent ?? ""
        self.strLatitute = "\(place.coordinate.latitude)"
        self.strLongitute = "\(place.coordinate.longitude)"
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude))
        mapView.animate(toZoom: 14)
    }
    
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didFailAutocompleteWithError error: Error) {
        // Handle the error.
        print("Error: \(error.localizedDescription)")
    }
    
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didSelect prediction: GMSAutocompletePrediction) -> Bool {
        
        self.tableView.isHidden = true
        self.searchBar.endEditing(false)
        return true
    }
}



protocol confirmLocationProtocol: AnyObject
{
    func locationSelected(cityDetail:CityDetail)
}


struct CityDetail: Codable {
    var address = ""
    var lat = ""
    var long = ""
}
