//
//  CommonShare.swift
//  MobiquityTest
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import UIKit

class CommonShare {
    
    static let ShareInsatnce = CommonShare()
    
    let userdefaults = UserDefaults.standard
    
    let keyConstatForCityList = "CityList"
    
   private init() {
        
    }

    
    func saveCityList(cityDetail: CityDetail)  {
        
        
        guard  var arr = getListFromUserDefaults() else {
            //for first time it will require to initialize the array
            let arr = [cityDetail]
            saveListToUserdefaults(arr: arr)
            return
        }
        
        arr.append(cityDetail)
        saveListToUserdefaults(arr: arr)
        
        
        
    }
    
    func removeCityFromList(index: Int)  {
        if  var arr = getListFromUserDefaults()
        {
            arr.remove(at:index)
            saveListToUserdefaults(arr: arr)
            
        }
     
    }
    
    
    func saveListToUserdefaults(arr : [CityDetail])  {
        do {
            try userdefaults.setObject(arr, forKey: keyConstatForCityList)
        } catch {
            print(error.localizedDescription)
        }
        userdefaults.synchronize()
    }
    
    
    func getListFromUserDefaults() -> [CityDetail]?  {
        
        do {
            let arr =   try userdefaults.getObject(forKey: keyConstatForCityList, castTo: [CityDetail].self)
            return arr
        } catch {
            print(error.localizedDescription)
        }
       
        
        return nil
    }
    
    
    
    

}


extension UserDefaults {
    
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            set(data, forKey: forKey)
        } catch {
            throw ObjectSavableError.unableToEncode
        }
    }
    
    func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable {
        guard let data = data(forKey: forKey) else { throw ObjectSavableError.noValue }
        let decoder = JSONDecoder()
        do {
            let object = try decoder.decode(type, from: data)
            return object
        } catch {
            throw ObjectSavableError.unableToDecode
        }
    }
}


enum ObjectSavableError: String, LocalizedError {
    case unableToEncode = "Unable to encode object into data"
    case noValue = "No data object found for the given key"
    case unableToDecode = "Unable to decode object into given type"
    
    var errorDescription: String? {
        rawValue
    }
}
