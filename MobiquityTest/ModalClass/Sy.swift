//
//  Sy.swift
//  Mobiquity
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import Foundation

struct Sy : Codable {

        let id : Int?
        let sunrise : Int?
        let sunset : Int?
        let type : Int?

        enum CodingKeys: String, CodingKey {
                case id = "id"
                case sunrise = "sunrise"
                case sunset = "sunset"
                case type = "type"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                sunrise = try values.decodeIfPresent(Int.self, forKey: .sunrise)
                sunset = try values.decodeIfPresent(Int.self, forKey: .sunset)
                type = try values.decodeIfPresent(Int.self, forKey: .type)
        }

}
