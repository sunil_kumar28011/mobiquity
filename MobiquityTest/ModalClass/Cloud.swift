//
//  Cloud.swift
//  Mobiquity
//
//  Created by Sunil Kumar Singh on 09/05/21.
//
import Foundation

struct Cloud : Codable {

        let all : Int?

        enum CodingKeys: String, CodingKey {
                case all = "all"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                all = try values.decodeIfPresent(Int.self, forKey: .all)
        }

}
