//
//  Coord.swift
//  Mobiquity
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import Foundation

struct Coord : Codable {

        let lat : Int?
        let lon : Int?

        enum CodingKeys: String, CodingKey {
                case lat = "lat"
                case lon = "lon"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                lat = try values.decodeIfPresent(Int.self, forKey: .lat)
                lon = try values.decodeIfPresent(Int.self, forKey: .lon)
        }

}
