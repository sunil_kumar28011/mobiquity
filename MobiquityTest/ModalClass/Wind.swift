//
//  Wind.swift
//  Mobiquity
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import Foundation

struct Wind : Codable {

        let deg : Int?
        let gust : Float?
        let speed : Float?

        enum CodingKeys: String, CodingKey {
                case deg = "deg"
                case gust = "gust"
                case speed = "speed"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                deg = try values.decodeIfPresent(Int.self, forKey: .deg)
                gust = try values.decodeIfPresent(Float.self, forKey: .gust)
                speed = try values.decodeIfPresent(Float.self, forKey: .speed)
        }

}
