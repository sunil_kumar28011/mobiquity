//
//  ViewController.swift
//  MobiquityTest
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tblVw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func addButtonClicked(_ sender: UIBarButtonItem) {
        
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MapView") as! MapViewController
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        
       
    }
    
}

extension ViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cityListCount = (CommonShare.ShareInsatnce.getListFromUserDefaults()?.count)
        return cityListCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        if cell == nil
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        let cityList = CommonShare.ShareInsatnce.getListFromUserDefaults()
     
        if let cityDetail = cityList?[indexPath.row]
       {
        cell?.textLabel?.text = cityDetail.address
       }
     
        cell?.textLabel?.numberOfLines = 0
        return cell!
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action = UIContextualAction(style: .normal,
                                        title: "Delete") { [weak self] (action, view, completionHandler) in
            CommonShare.ShareInsatnce.removeCityFromList(index: indexPath.row)
            self?.tblVw.reloadData()
            completionHandler(true)
        }
        action.backgroundColor = .systemBlue
        
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CityDetailView") as! CityDetailViewController
        
        let cityList = CommonShare.ShareInsatnce.getListFromUserDefaults()
     
        if let cityDetail = cityList?[indexPath.row]
       {
            controller.cityDetail = cityDetail
        }
       
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}

extension ViewController : confirmLocationProtocol
{
    func locationSelected(cityDetail: CityDetail) {
        CommonShare.ShareInsatnce.saveCityList(cityDetail: cityDetail)
        tblVw.reloadData()
    }
    
    
   
    
    
}

