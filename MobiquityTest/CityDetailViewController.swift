//
//  CityDetailViewController.swift
//  MobiquityTest
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import UIKit

class CityDetailViewController: UIViewController {

    var cityDetail: CityDetail?
    
    @IBOutlet weak var lblDetilData: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "City Detail"
        getCityDetail()
    }
    
    func setData(weatherData: WeatherMapRoot)  {
        self.lblDetilData.text = "Today's Temperature : \(weatherData.main?.temp ?? 0)\nToday's humidity : \(weatherData.main?.humidity ?? 0)\nToday's Rain Chances : \(weatherData.weather?.first?.main ?? "")\nWind Information : \(weatherData.wind?.speed ?? 0) "
    }
    
}


extension CityDetailViewController
{
    
    func getCityDetail()  {
        
        
        if Reachability.isConnectedToNetwork() {
            
            JustHUD.shared.showInView(view: self.view, withHeader: "Loading", andFooter: "Please wait...")
            
            
            WebserviceClass.ShareInsatnce.getApi(urlString: "http://api.openweathermap.org/data/2.5/weather?lat=\(cityDetail?.lat ?? "0")&lon=\(cityDetail?.long ?? "0")&appid=fae7190d7e6433ec3a45285ffcf55c86", completionHandler: {
                (data , error) in
                DispatchQueue.main.async {
                    JustHUD.shared.hide()
                }
                guard let data = data else {
                    return
                }
                
                if error == nil {
                    
                    do {
                        let response = try JSONDecoder().decode(WeatherMapRoot.self, from: data)
                        DispatchQueue.main.async {
                            self.setData(weatherData: response)
                        }
                        print(response)
                    }catch let error1 {
                        print(error1)
                    }
                    
                }else {
                    
                }
                
            })
            
        } else {
            
            print("Not internet connection")
        }
        
    }
    
}
