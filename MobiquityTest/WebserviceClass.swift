//
//  WebserviceClass.swift
//  Mobiquity
//
//  Created by Sunil Kumar Singh on 09/05/21.
//

import UIKit

class WebserviceClass {
    
    static let ShareInsatnce = WebserviceClass()
    
   private init() {
        
    }
    
    
    func PostApi(urlString: String, dict: NSMutableDictionary, completionHandler:@escaping (Data? , Error?) -> ()){
        
        print(urlString)
        print(dict)
        
        let postUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print("postUrl =\(postUrl ?? "")")
        
        var request = URLRequest(url:  URL(string: postUrl ?? "")!)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = try! JSONSerialization.data(withJSONObject: dict)
        
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 300.0
        sessionConfig.timeoutIntervalForResource = 600.0
        let session = URLSession(configuration: sessionConfig)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data ,response, error) in
          
                if(error == nil){
                    
                    completionHandler(data,nil)
                    
                }else{
                    
                    completionHandler(nil, error)
                }
            
            })
        task.resume()
        
        
        
    }
    
    
    
    
    func getApi(urlString: String, completionHandler:@escaping (Data? , Error?) -> ()) {
        
        let postUrl =  URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
      
        
        print("url =\(postUrl!)")
        
        
        var request = URLRequest(url: postUrl! as URL)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 300.0
        sessionConfig.timeoutIntervalForResource = 600.0
        let session = URLSession(configuration: sessionConfig)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data ,response, error) in
          
                if(error == nil){
                 
                    completionHandler(data,nil)
                }else{
        
                    completionHandler(nil, error)
                }
           
            
            
        })
        task.resume()
        
        
        
    }
    
    
    
}




